# Nginx Subdomain

```nginx
server {
  listen 80 default_server;
  listen [::]:80 default_server;

  ####################
  server_name test.fresult.dev;
  ####################

  location / {
    proxy_pass http://127.0.0.1:3000/;
  }
}
```
