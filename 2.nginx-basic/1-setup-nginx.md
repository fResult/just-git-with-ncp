# Setting up NGINX

## Install NGINX
```bash
sudo apt install nginx
```

## Start nginx service
```bash
sudo service nginx start
```

## See nginx config file
```bash
sudo less /etc/nginx/sites-available/default
# or
sudo less /etc/nginx/sites-enabled/default
```

## NGINX CONNECT to EXPRESS (`proxy_pass` directive)
```nginx
location / {
  proxy_pass <URL_TO_PROXY_TO>;
}

# or

location / {
  proxy_pass http://127.0.0.1:3000/;
}
```
