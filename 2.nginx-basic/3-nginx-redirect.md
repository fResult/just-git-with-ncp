# Nginx Redirect

When access http://test.fresult.dev (example page), It will redirect you to https://developer.mozilla.org/en-US with HTTP status code 301
```nginx
server {
  listen 80 default_server;
  listen [::]:80 default_server;

  server_name test.fresult.dev;
  location / {
    proxy_pass http://127.0.0.1:3000/;
  }

  #####################
  location /help {
    return 301 https://developer.mozilla.org/en-US/;
  }
  ####################
}
```
