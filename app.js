const express = require('express')
const cors = require('cors')

/**
 * @type import('express').Express
 */
const app = express()
const port = 3000

app.use(cors())

app.get('/', cors(), (req, res) => {
  res.json({ message: 'Hello, world' })
})

/**
 * @type import('http').Server
 */
const server = app.listen(port, () =>
  console.log(`Example app is listening on ${port}`)
)
