# NGINX SETUP

### INSTALL and START SERVICE
```bash
sudo apt install nginx

sudo service nginx start

sudo less /etc/nginx/sites-available/default
```



### NGINX CONNECT to EXPRESS (proxy_pass directive)
```nginx
location / {
   proxy_pass URL_TO_PROXY_TO;
}
```
And then Restart NGINX service.
```bash
sudo service nginx reload
```
