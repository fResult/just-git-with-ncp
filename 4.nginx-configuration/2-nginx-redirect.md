# NGINX REDIRECT

Put this block into `/etc/nginx/sites-available/default`

```nginx
location /help {
   return 301 https://developer.mozilla.org/en-US/;
}
```

then `sudo service nginx reload`

try to go to http://fResult/help,
it will redirect to https://developer.mozilla.org/en-US
