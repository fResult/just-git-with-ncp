# NGINX gzip
### Go to /etc/nginx/nginx.conf
```bash
sudo vim /etc/nginx/nginx.conf
```

| ##
| # Gzip Settings:
| ##
```nginx
gzip on;
gzip_disable "msie6";

# gzip_vary on;
# gzip_proxied any;
# gzip_comp_level 6;
# gzip_buffers 16 8k;
# gzip_http_version 1.1;
```
