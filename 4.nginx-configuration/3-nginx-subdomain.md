# NGINX Subdomain

Put this block into `/etc/nginx/sites-available/default`

```nginx
server {
  listen 80;
  listen [::]; #IPv6 notation

  server_name test.fResult.dev; #_;

  location / {
    proxy_pass http://localhost:3000
  }
}
```
