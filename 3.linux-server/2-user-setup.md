# User Setup

## Switch user
```bash
su <username>
# or
su root
# or
su my-user
```
## Add current user into some group
```bash
usermod -aG <group_name> <username>
# or (add `my-user` into `sudo` group)
usermod -aG sudo my-user
```

---------------
## User Management

### Disable user
```bash
sudo passwd -l <username>
# or disable my-user user
sudo passwd -l my-user
```

### Delete user
```bash
sudo passwd -r <username>
# or disable my-user user
sudo passwd -r my-user
```

### Add user to a user groupo
```bash
sudo usermod -a -G <group_name> <username> #or
sudo usermod -aG <group_name> <username>
# or disable my-user user
sudo usermod -aG my-group my-user
```

### Remove user from a user group
```bash
sudo deluser <username> <group_name>
# or disable my-user user
sudo deluser -aG my-user my-group
```

### Give an information on all logged in user
```bash
finger
```

### Give an information of a particular user
```bash
finger <username>
```
