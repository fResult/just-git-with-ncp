# Linux Server Setup

## Update Software
```bash
apt update
```

## Upgrade Software
```bash
apt upgrade
```

## Add new user
```bash
adduser <username>
# or
adduser my-user
```
