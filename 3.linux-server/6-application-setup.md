# Application SETUP

## CHANGE OWNERSHIP
```bash
sudo chown -R <username>:<username> /var/www
```
* Change ownership of the `www` directory to the current user

## CREATE APP DIRECTORY
```bash
mkdir /var/www/app
cd /var/www/app && git init
```