# Disabling the `root` user

## Change `authorized_keys` file permissions to 664
```bash
chmod 664 ~/.ssh/authorized_keys
```

## Disable root login
```bash
sudo vim /etc/ssh/sshd_config
```

### then change `PermitRootLogin` to be `no`
```conf
PermitRootLogin no
```

### then restart `sshd` service to apply above configuration
```bash
sudo service sshd restart
```

### then try to login ssh with root
```bash
ssh root@my-server
# or
ssh root@my-ip-address

# it will display output that `Permission denied (publickey)` cuz you already disalbed Root Login in `/etc/ssh/sshd_config` file
```
