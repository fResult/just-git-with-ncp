# Process Manager

## PM2
**PM2** is a Process Manager cli

## Installation
```bash
yarn global add pm2
```
## START PM2
```bash
pm2 start <executable_file_which_is_app_server>
# example
pm2 start /var/www/app/app.js
```

## STOP PM2
```bash
pm2 stop
```

## SETUP AUTO STARTUP WHEN SERVER IS START
```bash
pm2 startup
```
### then paste and copy and execute command which is output from `pm2 startup`
