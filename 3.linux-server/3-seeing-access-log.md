# Seeing Access log

## Authentication log for current Ubuntu server accesses
```bash
sudo cat /var/log/auth.log
```
## See `auth.log` follow as real time as possible with 100 lines
```bash
tail -f -l 100 /var/log/auth.log
```
