# Setting User Permissions

## Create a new `.ssh` directory if doesn't exist
```bash
mkdir -p ~/.ssh
```

## Create `authorized_keys` file and paste PUBLIC key
```bash
# On the Client, run this, to create private/public key `rsa` format
ssh-keygen -t rsa

# then read file `~/<your_key>.pub`, then copy an output
cat ~/<your_key>.pub

# On the Server, with NEW USER's logged in, type this, and paste PUBLIC key from client
vim ~/.ssh/authorized_keys
```

## Then exit from the server, and Login with the new user (my-user)
```bash
exit

# and
ssh my-user@my-server
#or
ssh my-user@my-ip-address
#or
ssh my-user@192.168.59.59
```

## Change `authorized_keys` file permissions to 664
```bash
# Change permission to be `-rw-rw-r—` (user rw, group rw, others r)
chmod 664 ~/.ssh/authorized_keys
```
