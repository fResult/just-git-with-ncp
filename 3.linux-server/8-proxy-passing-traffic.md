# Proxy Passing Traffic

## Open `nginx` configuration file with vim
```bash
sudo vim /etc/nginx/sites-available/default
# or
sudo vim /etc/nginx/sites-enabled/default
```

### then add below...
```nginx
location / {
  proxy_pass: http://localhost:3000/
}
```

### then restart `nginx` service
```bash
sudo service nginx reload
```

### and now we can access our app through web browser without port 3000
```bash
curl http://my-server/
# or
curl http://my-server-ip-address/

# then get response from app.js
## { message: 'hello world' }
```
