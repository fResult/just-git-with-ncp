# Searching File Contents with `grep` and `zgrep`

```bash
ps aux | grep node
```
We will have process running which is `node` if it is running.
* **| (pipe)** is redirection operator that read from `stdout` then find the word `node` with `grep` command.

Another example
```bash
sudo cat /var/log/auth.log | grep "my-user (uid)"
```
* **| (pipe)** is redirection operator that read from `stdout` then find the word `my-user (uid)` with `grep` command
