# Linux Standard Streams & Redirection

## Standard Streams
* Standard Output
  * stdout
* Standard Input
  * stdin
* Standard Error
  * stderr

## Redirection
* |
  * Read from stdout
* \>
  * Write stdout to file
* \>\>
  * Append stdout to file
* <
  * Read from stdin
* 2>
  * Read from stderr
