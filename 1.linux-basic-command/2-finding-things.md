# Finding Things
## find
Search file names
### Example:
```bash
find /bar - name foo.txt
```
* find - `find` command
* /bar - directory
* -name - option
* foo.txt - file/folder
### useful options
* -name
* -type
* -empty
* -executable
* -writable

```bash
sudo find /var/log/nginx/ -name "*.log" -type f
```
* Find all the files with the extension as `log`

```bash
sudo find / -type d -name log
```
* Find all the directories with the name `log`

## grep
Search file contents
### Example:
```bash
grep -i 'app' /var/www
```
* grep - `grep` command
* -i - options
* ‘app’ - search expression
* /var/www - directory

### useful options
* -i - search by insensitive case wording
* -E - extend Regular Expression

```bash
ps aux | grep -i 'Node'
```
* Find the `node` process from stdout of `ps` (if node is running it will display to see)
sudo cat /var/log/auth.log | grep -iE "[^-]kornzilla\(uid=0\)"
* Find wording in auth.log file as kornzill(uid=0) which not start withs “-“ (hyphen) and insensitive case

## zgrep
Search inside gzip file
