# Text manipulation

## wc command
for word, character, line count.

```bash
wc /etc/passwd #or
wc -cmlw /etc/passwd

# output
35 57 1698 /etc/passwd
```

```bash
wc -m /etc/passwd

#output
1698 /etc/passwd
```

```bash
wc -l /etc/passwd

#output
35 /etc/passwd
```

```bash
wc -c /etc/passwd

#output
1698 /etc/passwd
```

```bash
wc -w /etc/passwd

#output
57 /etc/passwd
```

## cut command
Cut is used for text processing. You can use this command to extract portion of text from a file by selecting columns. For most of the example, we’ll be using the following test file

```bash
cat test.txt

# output
cat command for file oriented operations.
cp command for copy files or directories.
ls command to list out files and directories with its attributes.
To extract only a desired column from a file use -c option. The following example displays 2nd character from each line of a file test.txt
```
### -c\<count\>
As seen below, the characters a, p, s are the second character from each line of the test.txt file
```bash
cut -c2 test.txt

# output
a
p
s
```

### -c\<range>
Range of characters can also be extracted from a file by specifying start and end position delimited with -.
The following example extracts first 3 characters of each line from a file called test.txt

```bash
cut -c1-3 test.txt

# output
cat
cp
ls
```

### Combination of `-f` and `-d`
You can combine option -f and -d. The option -f specifies which field you want to extract, and the option -d specifies what is the field delimiter that is used in the input file

The following example displays only first field of each lines from /etc/passwd file using the field delimiter : (colon). In this case, the 1st field is the username. The file

```bash
cut -d':' -f1 /etc/passwd #or
cut -d: -f1 /etc/passwd

# output
root
daemon
bin
sys
sync
games
```

You can also extract more than one fields from a file.
This following example will get column 1 and 6 that separated by `:` (colon)
```bash
cut -d':' -f1,6 /etc/passwd #or
cut -d: -f1,6 /etc/passwd

# output
root:/root
daemon:/usr/sbin
bin:/bin
sys:/dev
sync:/bin
games:/usr/games
```

To display the range of fields specify start field and end field as shown below.
In this example, we are selecting field 1, 2, 3, 4 and 7

```bash
cut -d':' -f1-4,7 /etc/passwd #or
cut -d: -f1-4,7 /etc/passwd

# output
root:x:0:0:/bin/bash
daemon:x:1:1:/usr/sbin/nologin
bin:x:2:2:/usr/sbin/nologin
sys:x:3:3:/usr/sbin/nologin
sync:x:4:65534:/bin/sync
games:x:5:60:/usr/sbin/nologin
