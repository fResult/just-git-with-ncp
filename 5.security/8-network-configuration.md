# Network Configuration

## Configure by CLI (temporarily config)
### Add IP Adress and refresh it (manual configure IP)
To temporarily configure an ip address, you can use the `ip` command in the following manner. Modify the ip address and subnet mark to match your network requirements.
```bash
sudo ip addr add 10.102.66.200/24 dev enp0s25 #or
sudo ip addr add 10.102.66.200 255.255.255.0 dev enp0s25 #or

# We should down and up your interface to `refresh` an ip configuration.
sudo ip link set dev enp0s25 down
sudo ip link set dev enp0s25 up
```

### Add default gateway
To configure a default gateway, you can use the `ip` command in the following manner. Modify the default gateway address to match your network requirements.
```bash
sudo ip route add default via 10.102.66.1
```

### Verify routing table
```bash
ip route show
```

---

When reboot the machine, above configuration will be disappear.
So, below configuration will be solve this problem.

---

## Configure by Interface File (permanently config)

### Where is the Interface File?
```bash
# Ubuntu old version
vim /etc/network/interface

# Ubuntu new version
vim /etc/netplan/00-installer-config.yaml #or
vim /etc/netplan/50-cloud-init.yaml #then
sudo netplan apply

# Fedora, CentOS
vim /etc/sysconfig/network-scripts
```

### IP forwarding configuration (only ubuntu)
```bash
sudo vim /etc/sysctl.conf

# then uncomment `net.ipv4.ip_forward=1` in this file
`# Uncomment the next line to enable packet forwarding for IPv4`
`#net.ipv4.ip_forward=1`
```
