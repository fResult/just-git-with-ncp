# Upgrading NodeJS

## Download setup script from nodesource
**curl (cURL)** - Client URL, it's one of common commands for just reading and writing external sources.

So, we're actually gonna connect directly to NodeSource, the Debian repository, and Debian is a flavor of linux that maintains these packages.
```bash
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
```

## Run script
```bash
sudo bash nodesource_setup.sh
```

## Install NodeJS
```bash
sudo apt install nodejs
```

## Explain Shell
If you're curious about command which you copy and run, you can see explaination of command at https://explainshell.com/explain?cmd=curl+-sL
```
https://explainshell.com/explain?
https://explainshell.com/explain?cmd=curl+-sL
```

## Upgrade outdate packages
```bash
sudo npm update -g
```
