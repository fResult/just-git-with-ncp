# Port scanning with `nmap`
## NMAP
nmap is a port scanner and it can run over an entire range of IP address and it just checks for open ports.

### Install nmap
```bash
sudo apt install nmap
```

### Run nmap
```bash
nmap <YOUR_SERVER_IP>
```

### Run nmap with more service/version info
```bash
nmap -sV <YOUR_SERVER_IP>
```
