# Read auth.log

```bash
sudo less -N /var/log/auth.log
sudo tail /var/log/auth.log
```

We can know location of IP Addresses...
https://www.maxmind.com/en/geoip-demo