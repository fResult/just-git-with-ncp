# PERMISSIONS

## list files and see permissions
```bash
ls -la
```

#### we will see some like these...
```
drwxr-xr-x  15 nipa-lt0045  staff    480 Aug 16 22:39 .git
-rw-r--r--   1 nipa-lt0045  staff     29 Aug 10 23:31 .gitignore
-rw-r--r--   1 nipa-lt0045  staff      4 Aug 12 18:57 .prettierignore
-rw-r--r--   1 nipa-lt0045  staff    211 Aug 11 00:02 .prettierrc
```
d - Directory
r - Readable
w - Writable
x - Executable

### digit of `d rwx`
d rwx rwx rwx
**1st rwx** for my self (U - User)
**2nd rwx** for my group (G - group)
**3rd rwx** for everyone (O - others)

## Markdown cheat sheet
https://isabelcastillo.com/linux-chmod-permissions-cheat-sheet

## Permission setting
```bash
chmod <permissions> <file_name>
chmod -R <permission> <directory>
```
**-R** - recursive

## chmod's points
1 = X
2 = W
4 = R

## types
**U** - **User**, which is the owner of file.
**G** - **Group**, which is a user-group that multiple users are contain in the group.
**O** - **Other**, which is any other user who has access to a file.
This person has neither created the file, nor he belongs to a user-group who could own the file
