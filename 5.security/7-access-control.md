# Access Control

## Adding User Accounts
To add a new user account, you need to run the following command as root to use sudo command
```bash
sudo adduser <username> #or
sudo useradd <username>

#example
sudo adduser test1
```

When a new user account is added to the system, the following operations are performed

1. His/her home directory is created (/home/username by default)
2. The following hidden files are copied into the user’s home directory, and will be used to provide environment variables for his/her user session
  - .bash_logouta
  - .bash_profile
  - .bashrc

3. A mail spool is created for the user at /var/spool/mail/username.

4. A group is created and given the same name as the new user account

You need to be root, or require escalated privileges for this process to work. The following screenshot shows a new user being added through this tool

## Deleting User Accounts
You can use the shell command deluser to delete a user. Here is an example
```bash
sudo deluser <username>
```

## chmod command
chmod stands for change mode, which changes the file or directory mode bits. To put it simply, use chmod command to change the file or directory permissions.
Following is a sample of ‘ls -l’ command output. In this, the 9 characters from 2nd to 10th position represents the permissions for the 3 types of users.

```bash
ls -l /etc/passwd

#output
-rw-r--r-- 1 root root 1611 Mar 16 17:03 /etc/passwd
```
In the above example:
*	User (current user) has read and write permission
*	Group has read permission
*	Others have read permission

Numeric values for the read, write and execute permissions:
*	read 4
*	write 2
*	execute 1

To have combination of permissions, add required numbers. For example, for read and write permission, it is 4+2 = 6.
Give read, write and execute to everybody (user, group, and others)
read, write and execute = 4 + 2 + 1 = 7.
```bash
chmod 777 file1.txt #or
chmod ugo+rwx file.txt

#output
-rwxrwxrwx 1 test1 test1 0 Aug 27 0:29 file1.txt
```

Give execute privilege to user. Leave other privileges untouched.
execute = 1. If you want to just add execute privilege to users and leave all other privileges as it is, do the following.
```bash
chmod u+x file2.txt

ls -l file2.txt

#output
-rwxrw-r-- 1 test1 test1 0 Aug 27 0:31 file2.txt
```

## chown command
Use the ‘ls -l’ command to find out who owns a file or what group the file belongs to.
```bash
ls -l file1.txt
#output
-rw-  r--   r-- 1 test1 test1 0 Mar 16 14:26 file1.txt
 |    |     |
 |    |     +------------> Other
 |    |
 |    +------------------> Group
 |
 +-----------------------> Owner
```

Only root or user with sudo privileges can change the group ownership of a file.
To change the owner of a file use the chown command followed by the user name of the new owner and the target file as an argument

```bash
sudo chown <username> <file>
```

For example, the following command will change the ownership of a file named file1 to a new owner named john.

```bash
sudo chown john file1.txt

#output
-rwxrwxrwx 1 john test1 0 Aug 27 0:29 file1.txt
```

To change the ownership of multiple files or directories, specify them as a space-separated list. The command below changes the ownership of a file named file1 and directory dir1 to a new owner named john.
```bash
sudo chown john file2.txt example/

#output
drwxrwxr-x 2 john test1 4096 Aug 27 0:31 example
-rwxrw-r-- 1 john test1    0 Aug 27 0:31 file2.txt
```

To change both the owner and the group of a file use the chown command followed by the new owner and group separated by a colon (:) with no intervening spaces and the target file
```bash
chown <username>:<group> <file>
```

The following command will change the ownership of a file named file1 to a new owner named john and group root.
```bash
sudo chown john:john file2.txt
-rwxrw-r-- 1 john john 0 Aug 27 0:31 file2.txt
```
