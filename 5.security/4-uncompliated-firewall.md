# Uncomplicated Firewall (ufw)

## ufw command

```bash
ufw {allow|deny|reject} {http|https|ssh}
```

It is less complicated syntax than `iptables` for example...

```bash
iptables -p tcp --dport 80 -j REJECT
```

## How to close port

### Check firewall status

```bash
sudo ufw status
sudo ufw status verbose # display status with opened port
```

### Enable firewall

```bash
sudo ufw enable # then all ports will be closed
```

### Enable ssh

```bash
sudo ufw allow ssh
```

### Enable HTTP & HTTPS

```bash
sudo ufw allow http && sudo ufw allow https
```
